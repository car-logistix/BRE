package de.johenneken.soa.bre.domain.dto;

public class InvoiceEscalationDTO {

    private Double total;
    private String supplier;

    private boolean overdue;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public boolean isOverdue() {
        return overdue;
    }

    public void setOverdue(boolean overdue) {
        this.overdue = overdue;
    }

    @Override
    public String toString() {
        return "InvoiceEscalationDTO{" +
            "total=" + total +
            ", supplier='" + supplier + '\'' +
            ", overdue=" + overdue +
            '}';
    }
}
