package de.johenneken.soa.bre.config;

import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamundaConfiguration {

    @Bean
    public DmnEngine provideDmnEngine() {
        return DmnEngineConfiguration.createDefaultDmnEngineConfiguration().buildEngine();
    }


}
