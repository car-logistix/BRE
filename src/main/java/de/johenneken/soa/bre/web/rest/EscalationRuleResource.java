package de.johenneken.soa.bre.web.rest;

import de.johenneken.soa.bre.domain.dto.InvoiceEscalationDTO;
import de.johenneken.soa.bre.service.EscalationRuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/rules")
public class EscalationRuleResource {
    Logger log = LoggerFactory.getLogger(EscalationRuleResource.class);

    private final EscalationRuleService escalationRuleService;


    public EscalationRuleResource(EscalationRuleService escalationRuleService) {
        this.escalationRuleService = escalationRuleService;
    }

    @RequestMapping(method = RequestMethod.POST,path ="/escalation" )
    public ResponseEntity<List<String>> evaluateInvoiceForContacts(@RequestBody InvoiceEscalationDTO invoiceEscalationDTO) throws IOException {
        log.debug("REST API call to evaluate a List of mail adresses for escalated Invoice");
        List<String> mailAdresses = escalationRuleService.determineContactAdressesForEscalatedInvoice(invoiceEscalationDTO);
        return ResponseEntity.ok(mailAdresses);
    }
}
