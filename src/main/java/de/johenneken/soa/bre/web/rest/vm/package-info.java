/**
 * View Models used by Spring MVC REST controllers.
 */
package de.johenneken.soa.bre.web.rest.vm;
