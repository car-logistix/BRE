package de.johenneken.soa.bre.service;

import de.johenneken.soa.bre.domain.dto.InvoiceEscalationDTO;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class EscalationRuleService {

    public static final String RULE_FILE = "invoiceEscalationDecision.dmn";
    private DmnEngine dmnEngine;
    private final Resource escalationDecision;

    EscalationRuleService(DmnEngine dmnEngine) {
        this.dmnEngine = dmnEngine;
        escalationDecision = new ClassPathResource(RULE_FILE);
    }

    public List<String> determineContactAdressesForEscalatedInvoice(InvoiceEscalationDTO invoiceEscalationDTO) throws IOException {
        InputStream inputStream = escalationDecision.getInputStream();
        DmnDecision decision = dmnEngine.parseDecision("escalation", inputStream);
        VariableMap variableMap = Variables
            .putValue("total",invoiceEscalationDTO.getTotal())
            .putValue("supplier",invoiceEscalationDTO.getSupplier())
            .putValue("overdue",invoiceEscalationDTO.isOverdue());
        DmnDecisionTableResult dmnDecisionRuleResults = dmnEngine.evaluateDecisionTable(decision, variableMap);
        return dmnDecisionRuleResults.collectEntries("address");
    }
}
